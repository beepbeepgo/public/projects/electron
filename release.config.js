const {
  semanticReleaseConfigDefault,
} = require("@beepbeepgo/semantic-release");
module.exports = semanticReleaseConfigDefault({
  plugins: {
    npm: {
      assets: ["package.json", "dist/**/*.js"],
    },
  },
});
